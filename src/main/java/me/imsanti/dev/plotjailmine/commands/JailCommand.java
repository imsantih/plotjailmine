package me.imsanti.dev.plotjailmine.commands;

import me.imsanti.dev.plotjailmine.PlotJailMine;
import me.imsanti.dev.plotjailmine.utils.PlayerUtils;
import me.imsanti.dev.plotjailmine.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class JailCommand implements CommandExecutor {

    private final PlotJailMine plotJailMine;

    public JailCommand(final PlotJailMine plotJailMine) {
        this.plotJailMine = plotJailMine;
    }
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("the console can't use the command");
            return true;
        }

        final Player player = (Player) sender;
        if(!(args.length == 2)) {
            player.sendMessage("Usage: /jail (player) (time)");
            return true;
        }

        final String playerName = args[0];

        if(!PlayerUtils.isValidPlayer(playerName)) {
            player.sendMessage("The player " + playerName + " is invalid");
            return true;
        }


        final Player target = Bukkit.getPlayer(playerName);

        if(!Utils.isValidInt(args[1])) {
            player.sendMessage(args[1] + " is not a number!");
            return true;
        }

        if(target == null) return true;

        plotJailMine.getJailManager().jail(target, Integer.parseInt(args[1]));
        return true;
    }
}
