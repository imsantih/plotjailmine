package me.imsanti.dev.plotjailmine;

import com.plotsquared.core.api.PlotAPI;
import me.imsanti.dev.plotjailmine.commands.JailCommand;
import me.imsanti.dev.plotjailmine.configs.ConfigManager;
import me.imsanti.dev.plotjailmine.listeners.PlayerListener;
import me.imsanti.dev.plotjailmine.managers.JailManager;
import me.imsanti.dev.plotjailmine.configs.MessagesF;
import me.imsanti.dev.plotjailmine.managers.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class PlotJailMine extends JavaPlugin {

    private final JailManager jailManager = new JailManager(this);
    private final PlayerManager playerManager = new PlayerManager(this);
    private final PlotAPI plotAPI = new PlotAPI();

    private final ConfigManager configManager = new ConfigManager(this);
    private final MessagesF messagesF = new MessagesF(configManager, this);

    @Override
    public void onEnable() {
        registerCommands();
        registerEvents();
        createConfigs();
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll();
        Bukkit.getOnlinePlayers().forEach(player -> {
            if(!jailManager.isJailed(player)) return;
            playerManager.saveData(player);
        });
        // Plugin shutdown logic
    }

    private void createConfigs() {
        if(messagesF.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created messages.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded messages.yml");
        }

    }

    private void registerCommands() {
        getCommand("jail").setExecutor(new JailCommand(this));
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(this, plotAPI), this);
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public JailManager getJailManager() {
        return jailManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public MessagesF getMessagesF() {
        return messagesF;
    }
}
