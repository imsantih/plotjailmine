package me.imsanti.dev.plotjailmine.objects;

import com.plotsquared.core.player.PlotPlayer;
import com.plotsquared.core.plot.Plot;
import org.bukkit.entity.Player;

public class JailedPlayer {

    private int blocksRemaining;
    private final int totalBlocks;
    private final Plot plot;
    private int minedBlocks;
    private int maxPlotsAllowed;
    private String gamemodeName;

    public JailedPlayer(final int blocksRemaining, final Plot plot)  {
        this.blocksRemaining = blocksRemaining;
        this.totalBlocks = blocksRemaining;
        this.plot = plot;
        this.minedBlocks = 0;
        this.gamemodeName = "CREATIVE";

    }


    public Plot getPlot() {
        return plot;
    }

    public void remove1Block() {
        this.blocksRemaining = blocksRemaining -1;
    }


    public int getBlocksRemaining() {
        return blocksRemaining;
    }

    public String getGamemodeName() {
        return gamemodeName;
    }

    public void setBlocksRemaining(int blocksRemaining) {
        this.blocksRemaining = blocksRemaining;
    }

    public void addMinedBlock() {
        minedBlocks++;
    }

    public int getTotalBlocks() {
        return totalBlocks;
    }

    public int getMinedBlocks() {
        return minedBlocks;
    }

    public int getMaxPlotsAllowed() {
        return maxPlotsAllowed;
    }
}
