package me.imsanti.dev.plotjailmine.utils;

import com.plotsquared.core.plot.Plot;
import me.imsanti.dev.plotjailmine.PlotJailMine;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class PlotUtils {

    public static com.plotsquared.core.location.Location getPlotLocation(Location location) {
        return new com.plotsquared.core.location.Location(location.getWorld().getName(), (int) location.getX(), (int) location.getY(), (int) location.getZ());
    }

    public static void addMarkFlag(final Plot plot) {
        new BukkitRunnable() {

            @Override
            public void run() {
                if(plot == null) {
                    Bukkit.getConsoleSender().sendMessage("Error. Line 24.");
                    return;
                }
                plot.addDenied(UUID.fromString("3c4e86be-57f4-4f45-8158-c6547211a498"));
            }
        }.runTaskLater(PlotJailMine.getPlugin(PlotJailMine.class), 40);
    }

    public static boolean isJailPlot(final Plot plot, final Player player) {
        return plot.getOwner().equals(player.getUniqueId()) && plot.isDenied(UUID.fromString("3c4e86be-57f4-4f45-8158-c6547211a498"));
    }


}
