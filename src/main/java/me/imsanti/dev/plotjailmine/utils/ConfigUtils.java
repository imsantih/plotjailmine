package me.imsanti.dev.plotjailmine.utils;

import me.imsanti.dev.plotjailmine.PlotJailMine;
import org.bukkit.ChatColor;

public class ConfigUtils {


    public static String getConfigMessage(final String value) {
        final PlotJailMine plotJailMine = PlotJailMine.getPlugin(PlotJailMine.class);

        return ChatColor.translateAlternateColorCodes('&', plotJailMine.getMessagesF().getConfig().getString(value) );
    }
}
