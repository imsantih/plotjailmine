package me.imsanti.dev.plotjailmine.utils;

import com.plotsquared.core.api.PlotAPI;
import me.imsanti.dev.plotjailmine.PlotJailMine;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static void timeTitle(final Player player, final String title, final int time) {
        new BukkitRunnable() {
            @Override
            public void run() {
                player.sendTitle(title, null, 5, 40, 10);
            }
        }.runTaskLater(PlotJailMine.getPlugin(PlotJailMine.class), 20L * time);
    }

    public static int getMaxPlotNumber(final Player player) {
        return new PlotAPI().wrapPlayer(player.getUniqueId()).getAllowedPlots();
    }

    public static boolean isValidInt(final String integerValue) {
        try {
            Integer.parseInt(integerValue);
            return true;
        }catch(Exception exception) {
            return false;
        }
    }

    public static List<File> readDataFiles() {
        final File folder = new File(PlotJailMine.getPlugin(PlotJailMine.class).getDataFolder() + "/players");
        File[] listOfFiles = folder.listFiles();
        final List<File> returnData = new ArrayList<>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                returnData.add(listOfFiles[i]);
            }
        }

        return returnData;
    }
}
