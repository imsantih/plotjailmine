package me.imsanti.dev.plotjailmine.utils;

import org.bukkit.Bukkit;

public class PlayerUtils {

    public static boolean isValidPlayer(final String playerName) {
        return Bukkit.getPlayer(playerName) != null && Bukkit.getPlayer(playerName).isOnline();
    }
}
