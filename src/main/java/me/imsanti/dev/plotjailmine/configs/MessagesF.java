package me.imsanti.dev.plotjailmine.configs;

import me.imsanti.dev.plotjailmine.PlotJailMine;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class MessagesF {

    private final PlotJailMine plotJailMine;

    public MessagesF(final ConfigManager configManager, final PlotJailMine plotJailMine) {
        this.configManager = configManager;
        this.plotJailMine = plotJailMine;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(plotJailMine.getDataFolder()), "messages", true)) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(plotJailMine.getDataFolder()), "messages");
    }

    public File getConfigFile() {
        return new File(plotJailMine.getDataFolder(), "messages.yml");
    }
}
