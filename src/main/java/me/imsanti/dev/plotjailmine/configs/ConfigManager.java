package me.imsanti.dev.plotjailmine.configs;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import me.imsanti.dev.plotjailmine.PlotJailMine;
import java.io.File;

public class ConfigManager {

    private final PlotJailMine plotJailMine;
    private File file;
    private FileConfiguration fileconfig;

    public ConfigManager(final PlotJailMine plotJailMine) {
        this.plotJailMine = plotJailMine;
    }

    public void saveConfigFile(File file, FileConfiguration fileConfiguration) {

        try {
            fileConfiguration.save(file);
            Bukkit.getConsoleSender().sendMessage("Configuration has been saved!");
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage("We can't reload your configuration!");
            e.printStackTrace();
        }
    }

    public boolean createConfigFile(String location, String filename, boolean isMessagesFile) {
        file = new File(location, filename + ".yml");

        if (!file.exists()) {
            try {
                if(!isMessagesFile) {
                    YamlConfiguration.loadConfiguration(file);
                    return true;
                }else {
                    plotJailMine.saveResource(filename + ".yml", false);
                    return true;
                }
            } catch(Exception exception) {
                exception.printStackTrace();
                return false;
            }
        }

        fileconfig = new YamlConfiguration();
        try {
            fileconfig.load(file);
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage("Error in trying to load the configuration!");
            e.printStackTrace();

        }

        return false;
    }


    public FileConfiguration getConfigFromFile(String location, String fileName) {
        File file = new File(location, fileName + ".yml");
        return YamlConfiguration.loadConfiguration(file);
    }
}
