package me.imsanti.dev.plotjailmine.configs;

import me.imsanti.dev.plotjailmine.PlotJailMine;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class PlayerDataFile {

    private final PlotJailMine plotJailMine;
    private final String playerName;
    private final ConfigManager configManager;

    public PlayerDataFile(ConfigManager configManager, final PlotJailMine plotJailMine, final String playerName) {
        this.configManager = configManager;
        this.plotJailMine = plotJailMine;
        this.playerName = playerName;
    }

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(plotJailMine.getDataFolder() + "/players"), playerName, false)) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(plotJailMine.getDataFolder() + "/players"), playerName);
    }

    public String getPlayerName() {
        return playerName;
    }

    public File getConfigFile() {
        return new File(plotJailMine.getDataFolder() + "/players", playerName + ".yml");
    }

    public boolean fileExists() {
        return new File(plotJailMine.getDataFolder() + "/players", playerName + ".yml").exists();
    }
}
