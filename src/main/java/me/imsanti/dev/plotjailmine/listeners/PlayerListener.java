package me.imsanti.dev.plotjailmine.listeners;

import com.google.common.eventbus.Subscribe;
import com.plotsquared.core.api.PlotAPI;
import com.plotsquared.core.events.PlayerEnterPlotEvent;
import com.plotsquared.core.plot.Plot;
import me.imsanti.dev.plotjailmine.PlotJailMine;
import me.imsanti.dev.plotjailmine.configs.PlayerDataFile;
import me.imsanti.dev.plotjailmine.objects.JailedPlayer;
import me.imsanti.dev.plotjailmine.utils.ConfigUtils;
import me.imsanti.dev.plotjailmine.utils.PlotUtils;
import me.imsanti.dev.plotjailmine.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private final PlotJailMine plotJailMine;


    public PlayerListener(final PlotJailMine plotJailMine, final PlotAPI plotAPI) {
        this.plotJailMine = plotJailMine;
        plotAPI.registerListener(this);
    }

    @EventHandler
    private void handlePlayerMine(final BlockBreakEvent event) {
        if(!plotJailMine.getJailManager().isJailed(event.getPlayer())) return;
        if(!((event.getBlock().getType()) == Material.STONE)) return;
        if(event.isCancelled()) return;
        //todo: descomentar la linea al terminar el plugin!
        //if(event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) return;
        final Player player = event.getPlayer();
        final JailedPlayer jailedPlayer = plotJailMine.getJailManager().getJailedPlayer(player);
        final Plot plot = PlotUtils.getPlotLocation(event.getPlayer().getLocation()).getPlot();
        if(plot == null) return;
        if(!plot.isOwner(event.getPlayer().getUniqueId())) return;
        if(!PlotUtils.isJailPlot(plot, event.getPlayer())) return;
        jailedPlayer.setBlocksRemaining(jailedPlayer.getBlocksRemaining() - 1);
        if(jailedPlayer.getBlocksRemaining() == 0) {
            plotJailMine.getJailManager().unjail(player);
            player.sendMessage(ConfigUtils.getConfigMessage("unjailed-message"));
            return;

        }
            jailedPlayer.addMinedBlock();
        Utils.timeTitle(player, ConfigUtils.getConfigMessage("block-mined-title").replaceAll("%remaining_blocks%", String.valueOf(jailedPlayer.getBlocksRemaining())).replaceAll("%total_blocks%", String.valueOf(jailedPlayer.getTotalBlocks())), 1);
        //player.sendTitle(jailedPlayer.getMinedBlocks() + "/" + jailedPlayer.getTotalBlocks() + " Blocks Mined", null, 1, 20, 1);
    }

    @Subscribe
    private void handlePlotEnterEvent(final PlayerEnterPlotEvent event) {
        if(Bukkit.getPlayer(event.getPlotPlayer().getUUID()) == null) return;

        final Player player = Bukkit.getPlayer(event.getPlotPlayer().getUUID());

//        Bukkit.broadcastMessage("tus máximas parcelas son: " + event.getPlotPlayer().getAllowedPlots());
        if(player == null) return;

        if(!plotJailMine.getJailManager().isJailed(player)) return;
        if(event.getPlot().getOwner() == null) return;
        //si no es owner de la plot, returneamos
        if(!event.getPlot().getOwner().equals(player.getUniqueId())) return;
        if(!PlotUtils.isJailPlot(event.getPlot(), player)) return;
        final JailedPlayer jailedPlayer = plotJailMine.getJailManager().getJailedPlayer(player);
        //es owner de la plot y esta jaileado, enviamos el titulo!
        Utils.timeTitle(player, ConfigUtils.getConfigMessage("block-mined-title").replaceAll("%remaining_blocks%", String.valueOf(jailedPlayer.getBlocksRemaining())).replaceAll("%total_blocks%", String.valueOf(jailedPlayer.getTotalBlocks())), 6);

        //Utils.timeTitle(player, jailedPlayer.getMinedBlocks() + "/" + jailedPlayer.getTotalBlocks() + " Blocks Mined", 2);
    }

    @EventHandler(ignoreCancelled = true)
    private void handleSaveInformation(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        if(!plotJailMine.getJailManager().isJailed(player)) return;
        plotJailMine.getPlayerManager().saveData(player);
    }

    @EventHandler(ignoreCancelled = true)
    private void handleCacheInformation(final PlayerJoinEvent event) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(plotJailMine.getConfigManager(), plotJailMine, event.getPlayer().getName());
        if(!playerDataFile.fileExists()) return;
        plotJailMine.getPlayerManager().loadData(event.getPlayer());
     }

}
