package me.imsanti.dev.plotjailmine.managers;

import com.plotsquared.core.location.Location;
import me.imsanti.dev.plotjailmine.PlotJailMine;
import me.imsanti.dev.plotjailmine.objects.JailedPlayer;
import me.imsanti.dev.plotjailmine.utils.PlotUtils;
import me.imsanti.dev.plotjailmine.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class JailManager {

    private final Map<UUID, JailedPlayer> jailedPlayers = new HashMap<>();
    private final PlotJailMine plotJailMine;

    public JailManager(final PlotJailMine plotJailMine) {
        this.plotJailMine = plotJailMine;
    }

    public void jail(final Player player, final int time) {
        player.performCommand("plot auto");

        new BukkitRunnable() {

            @Override
            public void run() {
                final Location location = PlotUtils.getPlotLocation(player.getLocation());
                player.performCommand("changeplotfloor STONE");
                final JailedPlayer playerJailed = new JailedPlayer(time, location.getPlot());
                PlotUtils.addMarkFlag(location.getPlot());
                jailedPlayers.put(player.getUniqueId(),playerJailed);
            }
        }.runTaskLater(plotJailMine, 40);
        player.getActivePotionEffects().forEach(effect -> {
            player.removePotionEffect(effect.getType());
        });
        plotJailMine.getPlayerManager().saveInventory(player);
        player.getInventory().clear();

        player.setGameMode(GameMode.SURVIVAL);
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission set plots.plot." + Utils.getMaxPlotNumber(player));
    }

    public void unjail(final Player player) {
        player.getInventory().clear();
        AtomicInteger i = new AtomicInteger();

        JailedPlayer jailedPlayer = jailedPlayers.get(player.getUniqueId());
        plotJailMine.getPlayerManager().loadInventory(player);
        player.setGameMode(GameMode.valueOf(jailedPlayer.getGamemodeName()));
        if(jailedPlayer.getPlot() == null) return;

        jailedPlayer.getPlot().deletePlot(new Runnable() {
            @Override
            public void run() {
                player.performCommand("back");
            }
        });
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission set plots.plot." + Utils.getMaxPlotNumber(player) + " false");
        jailedPlayers.remove(player.getUniqueId());

        final File file = new File(plotJailMine.getDataFolder() + "/players", player.getName());if(!file.exists()) return;
        file.delete();

    }

    public boolean isJailed(final Player player) {
        return jailedPlayers.containsKey(player.getUniqueId());
    }

    public JailedPlayer getJailedPlayer(final Player player) {
        return jailedPlayers.get(player.getUniqueId());
    }

    public Map<UUID, JailedPlayer> getJailedPlayers() {
        return jailedPlayers;
    }
}
