package me.imsanti.dev.plotjailmine.managers;

import me.imsanti.dev.plotjailmine.PlotJailMine;
import me.imsanti.dev.plotjailmine.configs.PlayerDataFile;
import me.imsanti.dev.plotjailmine.objects.JailedPlayer;
import me.imsanti.dev.plotjailmine.utils.Utils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private final PlotJailMine plotJailMine;
    private final Map<UUID, ItemStack[]> playersInventory = new HashMap<>();

    public PlayerManager(final PlotJailMine plotJailMine) {
        this.plotJailMine = plotJailMine;
    }

    public void saveInventory(final Player player) {
        playersInventory.put(player.getUniqueId(), player.getInventory().getContents());
    }

    public void loadInventory(final Player player) {
        if(!playersInventory.containsKey(player.getUniqueId())) return;
        player.getInventory().setContents(playersInventory.get(player.getUniqueId()));
    }

    public void saveData(final Player player) {
        JailedPlayer jailedPlayer = plotJailMine.getJailManager().getJailedPlayer(player);
        if(jailedPlayer == null) return;

        final PlayerDataFile playerDataFile = new PlayerDataFile(plotJailMine.getConfigManager(), plotJailMine, player.getName());
        playerDataFile.createConfigFile();

        final FileConfiguration config = playerDataFile.getConfig();

        config.set("totalBlocks", jailedPlayer.getTotalBlocks());
        config.set("blocksRemaining", jailedPlayer.getBlocksRemaining());
        config.set("blocksMined", jailedPlayer.getMinedBlocks());
        config.set("gamemode", jailedPlayer.getGamemodeName());

        try {
            config.save(playerDataFile.getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadData(final Player player) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(plotJailMine.getConfigManager(), plotJailMine, player.getName());
        final FileConfiguration config = playerDataFile.getConfig();
        if(config == null) return;

        plotJailMine.getJailManager().getJailedPlayers().put(player.getUniqueId(), new JailedPlayer(config.getInt("blocksRemaining"), null));
    }
//
//    public void loadAllData() {
//        Utils.readDataFiles().forEach(file -> {
//            final PlayerDataFile playerDataFile = new PlayerDataFile(plotJailMine.getConfigManager(), plotJailMine, file.getName().replaceAll(".yml", ""));
//
//            final FileConfiguration config = playerDataFile.getConfig();
//            final JailedPlayer jailedPlayer = new JailedPlayer(config.getInt("blocksRemaining"), null, )
//        });
//    }
}
